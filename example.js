const puppeteer = require('puppeteer-core');

(async () => {
  const browser = await puppeteer.launch({
      executablePath: '/usr/bin/chromium-browser', 
      args: [ '--use-fake-ui-for-media-stream']
    });
  const page = await browser.newPage();
  await page.goto('https://signaling.gameclient.me:4000/stream.html');
})();